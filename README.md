# Chat Micro Service

This micro service is responsible for handling all requests related to real-time chat.

### Instructions

1. Clone the repo 

2. Clone Main Server repo https://gitlab.com/gitrekt-msa/starter-server.git 

3. Clone dev-conf repo https://gitlab.com/gitrekt-msa/dev-conf.git

4. Run `docker-compose up` to start the server and dev-conf

2. Run `docker-compose up` to start the service

3. Access the server [http://localhost:3000](http://localhost:3000)