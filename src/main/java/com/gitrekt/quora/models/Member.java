package com.gitrekt.quora.models;

public class Member {
  private String username;

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public Member(){}

  public Member(String username) {
    this.username = username;
  }

  @Override
  public String toString() {
    return "Member{" + "username='" + username + '\'' + '}';
  }
}
