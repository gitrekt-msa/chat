package com.gitrekt.quora.models;

import io.netty.channel.ChannelHandlerContext;

public class Socket {

  private String userName;
  private String userId;
  private String socketId;
  private ChannelHandlerContext socketChannel;

  /**
   * Constructor for the socket.
   * @param userName username of the user
   * @param userId id of the user
   * @param socketId socket id
   * @param socketChannel socketChannel
   */
  public Socket(
      String userName, String userId, String socketId, ChannelHandlerContext socketChannel) {
    this.userName = userName;
    this.userId = userId;
    this.socketId = socketId;
    this.socketChannel = socketChannel;
  }

  public String getUserName() {
    return userName;
  }

  public String getUserId() {
    return userId;
  }

  public String getSocketId() {
    return socketId;
  }

  public ChannelHandlerContext getSocketChannel() {
    return socketChannel;
  }

  @Override
  public String toString() {
    return "Socket{"
        + "userName='"
        + userName
        + '\''
        + ", userId='"
        + userId
        + '\''
        + ", socketId='"
        + socketId
        + '\''
        + ", socketChannel="
        + socketChannel
        + '}';
  }
}
