package com.gitrekt.quora.models;

public class Tuple<X, Y> {
  public final X groupChatDetails;
  public final Y groupChatMessages;

  public Tuple(X firstParameter, Y secondParameter) {
    this.groupChatDetails = firstParameter;
    this.groupChatMessages = secondParameter;
  }
}
