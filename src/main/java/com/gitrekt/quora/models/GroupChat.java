package com.gitrekt.quora.models;

import com.arangodb.entity.DocumentField;

import java.util.ArrayList;

public class GroupChat {

  @DocumentField(DocumentField.Type.KEY)
  private String chatId;

  private String groupName;
  private String chatAdmin;
  private ArrayList<Member> chatMembers;

  public String getChatId() {
    return chatId;
  }

  public String getGroupName() {
    return groupName;
  }

  public String getChatAdmin() {
    return chatAdmin;
  }

  public ArrayList<Member> getChatMembers() {
    return chatMembers;
  }

  @Override
  public String toString() {
    return "GroupChat{"
        + "chatId='"
        + chatId
        + '\''
        + ", groupName='"
        + groupName
        + '\''
        + ", chatAdmin='"
        + chatAdmin
        + '\''
        + ", chatMembers="
        + chatMembers
        + '}';
  }
}
