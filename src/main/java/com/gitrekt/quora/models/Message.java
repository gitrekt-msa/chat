package com.gitrekt.quora.models;

import java.sql.Timestamp;

public class Message {
  public enum Type {
    Personal_Chat,
    Group_Chat
  }

  private String chatId;
  private String username;
  private String text;
  private Timestamp timestamp;
  private Type chatType;

  /** Super Constructor for the class Message. */
  public Message() {
    super();
  }

  /** Constructor for the class Message. */
  public Message(String chatId, String username, String text, Timestamp timestamp, Type chatType) {
    this.chatId = chatId;
    this.username = username;
    this.text = text;
    this.timestamp = timestamp;
    this.chatType = chatType;
  }

  public String getChatId() {
    return chatId;
  }

  public String getUsername() {
    return username;
  }

  public String getText() {
    return text;
  }

  public Timestamp getTimestamp() {
    return timestamp;
  }

  public Type getChatType() {
    return chatType;
  }

  @Override
  public String toString() {
    return "Message{"
        + "chatId='"
        + chatId
        + '\''
        + ", username='"
        + username
        + '\''
        + ", text='"
        + text
        + '\''
        + ", timestamp="
        + timestamp
        + ", chatType="
        + chatType
        + '}';
  }
}
