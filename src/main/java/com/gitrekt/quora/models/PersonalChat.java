package com.gitrekt.quora.models;

import com.arangodb.entity.DocumentField;

public class PersonalChat {
  @DocumentField(DocumentField.Type.KEY)
  private String chatId;
  private String username1;
  private String username2;

  public String getChatId() {
    return chatId;
  }

  public String getUsername1() {
    return username1;
  }

  public String getUsername2() {
    return username2;
  }

  @Override
  public String toString() {
    return "PersonalChat{"
        + "chatId='"
        + chatId
        + '\''
        + ",username1='"
        + username1
        + '\''
        + ", username2='"
        + username2
        + '\''
        + '}';
  }
}
