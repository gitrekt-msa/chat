package com.gitrekt.quora.controller;

import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.config.Config;

import java.lang.reflect.Constructor;
import java.util.HashMap;

// Would be better to store all commands in a Map commandName -> Command
public final class Invoker {

  private static final Config CONFIG = Config.getInstance();

  /**
   * Run the command using reflection.
   */
  public static Object invoke(String commandName, HashMap<String, Object> arguments)
          throws Exception {

    String commandClassPath =
            CONFIG.getProperty("commands") + "." + CONFIG.getProperty(commandName);
    Class<?> commandClass = getClass(commandClassPath);
    Constructor<?> commandConstructor = getConstructor(commandClass, HashMap.class);

    Command command = (Command) commandConstructor.newInstance(arguments);

    return command.execute();
  }

  private static Class<?> getClass(String classPath) throws ClassNotFoundException {
    return Class.forName(classPath);
  }

  private static Constructor<?> getConstructor(Class<?> clazz, Class<?>... parameterTypes)
          throws NoSuchMethodException {

    return clazz.getConstructor(parameterTypes);
  }
}
