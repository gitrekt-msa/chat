package com.gitrekt.quora.socket;

/*
 * Copyright 2012 The Netty Project
 *
 * The Netty Project licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.util.CharsetUtil;

/** Generates the demo HTML page which is served at http://localhost:8080/. */
public final class WebSocketServerIndexPage {

  private static final String NEWLINE = "\r\n";

  /**
   * An HTML test for the websocket realtime chat.
   *
   * @param webSocketLocation webSocketLocation
   * @return ByteBuf
   */
  public static ByteBuf getContent(String webSocketLocation) {
    return Unpooled.copiedBuffer(
        "<html><head><title>Web Socket Test</title></head>"
            + NEWLINE
            + "<body>"
            + NEWLINE
            + "<script type=\"text/javascript\">"
            + NEWLINE
            + "var socket;"
            + NEWLINE
            + "if (!window.WebSocket) {"
            + NEWLINE
            + "  window.WebSocket = window.MozWebSocket;"
            + NEWLINE
            + '}'
            + NEWLINE
            + "if (window.WebSocket) {"
            + NEWLINE
            + "  socket = new WebSocket(\""
            + webSocketLocation
            + "\");"
            + NEWLINE
            + "  socket.onmessage = function(event) {"
            + NEWLINE
            + "    var ta = document.getElementById('responseText');"
            + NEWLINE
            + "    ta.value = ta.value + '\\n' + event.data"
            + NEWLINE
            + "  };"
            + NEWLINE
            + "  socket.onopen = function(event) {"
            + NEWLINE
            + "    var ta = document.getElementById('responseText');"
            + NEWLINE
            + "    ta.value = \"Web Socket opened!\";"
            + NEWLINE
            + "  };"
            + NEWLINE
            + "  socket.onclose = function(event) {"
            + NEWLINE
            + "    var ta = document.getElementById('responseText');"
            + NEWLINE
            + "    ta.value = ta.value + \"Web Socket closed\"; "
            + NEWLINE
            + "  };"
            + NEWLINE
            + "} else {"
            + NEWLINE
            + "  alert(\"Your browser does not support Web Socket.\");"
            + NEWLINE
            + '}'
            + NEWLINE
            + NEWLINE
            + "function chat(userMessage,groupId) {"
            + NEWLINE
            + "  if (!window.WebSocket) { return; }"
            + NEWLINE
            + "  if (socket.readyState == WebSocket.OPEN) {"
            + NEWLINE
            + "    socket.send(JSON.stringify({"
            + NEWLINE
            + "  type: \"personalChat\","
            + NEWLINE
            + "  chatId: groupId,"
            + NEWLINE
            + "  message: userMessage"
            + NEWLINE
            + "}));"
            + NEWLINE
            + "  } else {"
            + NEWLINE
            + "    alert(\"The socket is not open.\");"
            + NEWLINE
            + "  }"
            + NEWLINE
            + '}'
            + NEWLINE
            + "function groupChat(userMessage,groupId) {"
            + NEWLINE
            + "  if (!window.WebSocket) { return; }"
            + NEWLINE
            + "  if (socket.readyState == WebSocket.OPEN) {"
            + NEWLINE
            + "    socket.send(JSON.stringify({"
            + NEWLINE
            + "  type: \"groupChat\" ,"
            + NEWLINE
            + "  chatId: groupId ,"
            + NEWLINE
            + "  message: userMessage"
            + NEWLINE
            + "}));"
            + NEWLINE
            + "  } else {"
            + NEWLINE
            + "    alert(\"The socket is not open.\");"
            + NEWLINE
            + "  }"
            + NEWLINE
            + '}'
            + NEWLINE
            + "function sendUsername(userName, userID) {"
            + NEWLINE
            + "  if (!window.WebSocket) { return; }"
            + NEWLINE
            + "  if (socket.readyState == WebSocket.OPEN) {"
            + NEWLINE
            + "    socket.send(JSON.stringify({"
            + NEWLINE
            + "  type: \"username\" ,"
            + NEWLINE
            + "  username: userName ,"
            + NEWLINE
            + " userId: userID"
            + NEWLINE
            + "}));"
            + NEWLINE
            + "document.getElementById(\"firstDiv\").style.display = \"none\";;"
            + NEWLINE
            + "document.getElementById(\"name\").textContent = userName;"
            + NEWLINE
            + "document.getElementById(\"secondDiv\").style.display = \"block\";;"
            + NEWLINE
            + "  } else {"
            + NEWLINE
            + "    alert(\"The socket is not open.\");"
            + NEWLINE
            + "  }"
            + NEWLINE
            + '}'
            + NEWLINE
            + "</script>"
            + NEWLINE
            + "<div id=\"firstDiv\">"
            + NEWLINE
            + "<form onsubmit=\"return false;\">"
            + NEWLINE
            + "<input type=\"text\" name=\"senderUsername\" placeholder=\"Enter your username..\"/>"
            + NEWLINE
            + "<input type=\"text\" name=\"senderId\" placeholder=\"Enter your userId..\"/>"
            + NEWLINE
            + "<input type=\"button\" value=\"Submit username\""
            + NEWLINE
            + "onclick=\"sendUsername(this.form.senderUsername.value,this.form.senderId.value)\" />"
            + NEWLINE
            + "</form>"
            + NEWLINE
            + "</div>"
            + NEWLINE
            + "<div id=\"secondDiv\" style=\"display:none;\">"
            + NEWLINE
            + "<h3 id=\"name\"></h3>"
            + NEWLINE
            + "<form onsubmit=\"return false;\">"
            + NEWLINE
            + "<input type=\"text\" name=\"message\" placeholder=\"message input\"/>"
            + NEWLINE
            + "<input type=\"text\" name=\"groupId\" placeholder=\"group Id\"/>"
            + NEWLINE
            + "<input type=\"button\" value=\"Send personal chat\""
            + NEWLINE
            + "       onclick=\"chat(this.form.message.value,this.form.groupId.value)\" />"
            + NEWLINE
            + "<input type=\"button\" value=\"Send group chat\""
            + NEWLINE
            + "       onclick=\"groupChat(this.form.message.value,this.form.groupId.value)\" />"
            + NEWLINE
            + "<h3>Output</h3>"
            + NEWLINE
            + "<textarea id=\"responseText\" style=\"width:500px;height:300px;\"></textarea>"
            + NEWLINE
            + "</form>"
            + NEWLINE
            + "</div>"
            + NEWLINE
            + "</body>"
            + NEWLINE
            + "</html>"
            + NEWLINE,
        CharsetUtil.US_ASCII);
  }

  private WebSocketServerIndexPage() {
    // Unused
  }
}
