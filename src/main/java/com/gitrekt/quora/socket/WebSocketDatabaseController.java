package com.gitrekt.quora.socket;

import com.gitrekt.quora.database.arango.handlers.GroupChatArangoHandler;
import com.gitrekt.quora.database.arango.handlers.PersonalChatHandler;
import com.gitrekt.quora.models.Message;

import java.util.concurrent.CopyOnWriteArrayList;

public class WebSocketDatabaseController implements Runnable {

  private static CopyOnWriteArrayList<Message> databaseMessages = new CopyOnWriteArrayList<>();

  public static CopyOnWriteArrayList<Message> getDatabaseMessages() {
    return databaseMessages;
  }

  @Override
  public void run() {
    new GroupChatArangoHandler().addMultipleMessages(databaseMessages);
    databaseMessages.clear();
  }
}
