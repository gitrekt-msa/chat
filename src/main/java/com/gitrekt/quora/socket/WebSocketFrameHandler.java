package com.gitrekt.quora.socket;

/*
 * Copyright 2012 The Netty Project
 *
 * The Netty Project licenses this file to you under the Apache License,
 * version 2.0 (the "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at:
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

import com.gitrekt.quora.database.arango.handlers.GroupChatArangoHandler;
import com.gitrekt.quora.database.arango.handlers.PersonalChatHandler;
import com.gitrekt.quora.models.Message;
import com.gitrekt.quora.pooling.ThreadPool;
import com.gitrekt.quora.queue.MessageQueueConnection;
import com.gitrekt.quora.queue.MessageQueueConsumer;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import io.netty.handler.codec.http.websocketx.WebSocketFrame;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeoutException;
import java.util.logging.Logger;

/** Echoes uppercase content of text frames. */
public class WebSocketFrameHandler extends SimpleChannelInboundHandler<WebSocketFrame> {

  private static final Logger LOGGER = Logger.getLogger(MessageQueueConsumer.class.getName());

  @Override
  protected void channelRead0(ChannelHandlerContext ctx, WebSocketFrame frame) throws Exception {
    // ping and pong frames already handled

    if (frame instanceof TextWebSocketFrame) {

      // Send the uppercase string back.
      String request = ((TextWebSocketFrame) frame).text();

      JsonObject requestBody = new JsonParser().parse(request).getAsJsonObject();

      String requestType = requestBody.get("type").getAsString();
      String senderUserName;
      String senderUserId;
      String messageText;
      String groupchatId;
      String receiverUserName;
      String receiverId;
      ChannelHandlerContext userCtx;

      switch (requestType) {
        case "username":
          senderUserName = requestBody.get("username").getAsString();
          senderUserId = requestBody.get("userId").getAsString();
          WebSocketController.addSocket(
              senderUserName, senderUserId, ctx.channel().id().toString(), ctx);
          break;
        case "groupChat":
          messageText = requestBody.get("message").getAsString();
          groupchatId = requestBody.get("chatId").getAsString();
          try {
            ArrayList<String> groupMembers =
                new GroupChatArangoHandler().getGroupChatMembers(groupchatId);

            ctx.channel().writeAndFlush(new TextWebSocketFrame(messageText));

            String username = WebSocketController.getUsername(ctx.channel().id().toString());
            String userId = WebSocketController.getUserId(username).toString();

            for (int i = 0; i < groupMembers.size(); i++) {
              userCtx = WebSocketController.getUserChannel(groupMembers.get(i));
              receiverId = WebSocketController.getUserId(groupMembers.get(i));
              if (userCtx != null && !groupMembers.get(i).equals(username)) {
                userCtx.channel().writeAndFlush(new TextWebSocketFrame(messageText));
                notifyUser(receiverId, userId);
              }
            }

            Message message =
                new Message(
                    groupchatId,
                    username,
                    messageText,
                    new Timestamp(new Date().getTime()),
                    Message.Type.Group_Chat);
            WebSocketDatabaseController.getDatabaseMessages().add(message);
          } catch (Exception exception) {
            ctx.channel().writeAndFlush(new TextWebSocketFrame("This groupchat doesnot exist!"));
          }
          break;
        case "personalChat":
          messageText = requestBody.get("message").getAsString();
          String personalChatId = requestBody.get("chatId").getAsString();

          String senderUsername = WebSocketController.getUsername(ctx.channel().id().toString());
          String senderId = WebSocketController.getUserId(senderUsername);

          receiverUserName = new PersonalChatHandler()
                  .getChatMember(personalChatId, senderUsername);
          receiverId = WebSocketController.getUserId(receiverUserName);

          notifyUser(receiverId, senderId);
          userCtx = WebSocketController.getUserChannel(receiverUserName);

          if (userCtx != null) {
            userCtx.channel().writeAndFlush(new TextWebSocketFrame(messageText));
          }
          ctx.channel().writeAndFlush(new TextWebSocketFrame(messageText));

          Message personalMessage = new Message(personalChatId, senderUsername, messageText,
                  new Timestamp(new Date().getTime()), Message.Type.Personal_Chat);
          WebSocketDatabaseController.getDatabaseMessages().add(personalMessage);
          break;
        default:
          return;
      }
    } else {
      String message = "unsupported frame type: " + frame.getClass().getName();
      throw new UnsupportedOperationException(message);
    }
  }

  /**
   * Putting notification in the queue for the message.
   *
   * @param receiverId receiverID
   * @param senderId senderID
   */
  public static void notifyUser(String receiverId, String senderId) {
    Runnable runnable =
        () -> {
          try {
            final AMQP.BasicProperties replyProperties =
                new AMQP.BasicProperties.Builder().replyTo(" ").build();
            JsonObject response = new JsonObject();
            response.addProperty("command", "getNotificationV1");
            JsonObject body = new JsonObject();
            body.addProperty("type", "new_message");
            body.addProperty("userId", receiverId);
            body.addProperty("senderId", senderId);

            response.add("body", body);

            Channel channel = MessageQueueConnection.getInstance().createChannel();
            channel.basicPublish(
                "",
                "notification-v1-queue",
                replyProperties,
                response.toString().getBytes(StandardCharsets.UTF_8));
            channel.close();
          } catch (IOException | TimeoutException exception) {
            LOGGER.severe(
                String.format(
                    "Error sending the response to main server\n%s", exception.getMessage()));
          } catch (Exception exception) {
            exception.printStackTrace();
          }
        };
    ThreadPool.getInstance().run(runnable);
  }
}
