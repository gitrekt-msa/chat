package com.gitrekt.quora.socket;

import com.gitrekt.quora.models.Socket;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;

import java.util.ArrayList;

public class WebSocketController {

  public static ArrayList<Socket> sockets = new ArrayList<Socket>();

  /**
   * Adding new socket.
   *
   * @param userName of the user
   * @param socketId of the socketID
   * @param ctx of the channel
   */
  public static void addSocket(
      String userName, String userId, String socketId, ChannelHandlerContext ctx) {

    for (int i = 0; i < sockets.size(); i++) {
      if (sockets.get(i).getSocketId().equals(socketId)) {
        return;
      }
    }
    Socket socket = new Socket(userName, userId, socketId, ctx);
    sockets.add(socket);

    ctx.channel().writeAndFlush(new TextWebSocketFrame("You are Registered!"));

    System.out.println("member added :" + sockets);
  }

  /**
   * Getting the username of the user from the socketId.
   *
   * @param socketId socketId
   * @return a string of a username
   */
  public static String getUsername(String socketId) {

    for (int i = 0; i < sockets.size(); i++) {
      if (sockets.get(i).getSocketId().equals(socketId)) {
        return sockets.get(i).getUserName();
      }
    }
    return null;
  }

  /**
   * search for the userId from the username.
   * @param username username of the user
   * @return the userID of this username
   */
  public static String getUserId(String username) {
    for (int i = 0; i < sockets.size(); i++) {
      if (sockets.get(i).getUserName().equals(username)) {
        return sockets.get(i).getUserId();
      }
    }
    return null;
  }

  /**
   * Remove a certain socket from its id.
   *
   * @param socketId socketId
   */
  public static void removeSocket(String socketId) {

    for (int i = 0; i < sockets.size(); i++) {
      if (sockets.get(i).getSocketId().equals(socketId)) {
        sockets.remove(i);
        System.out.println("member removed :" + sockets);
        return;
      }
    }
  }

  /**
   * Get the channel of the user that the message will be send on.
   *
   * @param username username
   * @return ChannelHandlerContext
   */
  public static ChannelHandlerContext getUserChannel(String username) {
    for (int i = 0; i < sockets.size(); i++) {
      if (sockets.get(i).getUserName().equals(username)) {
        return sockets.get(i).getSocketChannel();
      }
    }
    return null;
  }
}
