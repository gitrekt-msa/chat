package com.gitrekt.quora.commands;

import com.gitrekt.quora.database.arango.handlers.ArangoHandler;
import com.gitrekt.quora.database.postgres.handlers.PostgresHandler;
import com.gitrekt.quora.exceptions.BadRequestException;

import java.util.HashMap;

public abstract class Command {

  protected HashMap<String, Object> args;

  private PostgresHandler<?> postgresHandler;

  private ArangoHandler<?> arangoHandler;

  public Command(HashMap<String, Object> args) {
    this.args = args;
  }

  public void setPostgresHandler(PostgresHandler<?> postgresHandler) {
    this.postgresHandler = postgresHandler;
  }

  public void setArangoHandler(ArangoHandler<?> arangoHandler) {
    this.arangoHandler = arangoHandler;
  }

  protected void checkArguments(String[] requiredArgs) throws BadRequestException {
    StringBuilder stringBuilder = new StringBuilder();
    for (String argument : requiredArgs) {
      if (!args.containsKey(argument) || args.get(argument) == null) {
        System.out.println(argument);
        stringBuilder.append(String.format("Argument %s is missing", argument)).append("\n");
      }
    }
    if (stringBuilder.length() > 0) {
      System.out.println("str:" + stringBuilder.toString());
      throw new BadRequestException(stringBuilder.toString());
    }
  }

  public abstract Object execute() throws Exception;
}
