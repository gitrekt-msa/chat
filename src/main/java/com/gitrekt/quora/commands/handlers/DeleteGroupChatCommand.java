package com.gitrekt.quora.commands.handlers;

import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.config.Config;
import com.gitrekt.quora.database.arango.handlers.GroupChatArangoHandler;
import com.google.gson.Gson;
import com.google.gson.JsonElement;

import java.util.HashMap;

public class DeleteGroupChatCommand extends Command {

  private static final String[] argumentNames = new String[]{"chatId", "chatAdmin"};
  private static final Config CONFIG = Config.getInstance();

  public DeleteGroupChatCommand(HashMap<String, Object> args) {
    super(args);
  }

  @Override
  public JsonElement execute() throws Exception {
    checkArguments(argumentNames);

    String chatId = (String) args.get("chatId");
    String chatAdmin = (String) args.get("chatAdmin");


    String handlerClassPath =
            CONFIG.getProperty("arango") + ".GroupChatArangoHandler";
    Class handler =
            Class.forName(handlerClassPath);
    String message1 = ((GroupChatArangoHandler) handler.getConstructor().newInstance())
            .deleteGroupChat(chatId, chatAdmin);

    String message = "{\"message\":\""+message1+"\"}";
    Gson gson = new Gson();

    JsonElement messageElement = gson.fromJson(message, JsonElement.class);
    return messageElement;
  }

}
