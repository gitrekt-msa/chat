package com.gitrekt.quora.commands.handlers;

import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.config.Config;
import com.gitrekt.quora.database.arango.handlers.PersonalChatHandler;
import com.google.gson.Gson;
import com.google.gson.JsonElement;

import java.util.HashMap;

public class DeletePersonalChatCommand extends Command {

  private static final String[] argumentNames = new String[]{"chatId"};
  private static final Config CONFIG = Config.getInstance();

  public DeletePersonalChatCommand(HashMap<String, Object> args) {
    super(args);
  }

  @Override
  public JsonElement execute() throws Exception {
    checkArguments(argumentNames);

    String chatId = (String) args.get("chatId");

    String handlerClassPath =
            CONFIG.getProperty("arango") + ".PersonalChatHandler";
    Class handler =
            Class.forName(handlerClassPath);
    String response = ((PersonalChatHandler) handler.getConstructor().newInstance())
            .deletePersonalChat(chatId);

    String message = "{\"message\":\"" + response + "\"}";
    Gson gson = new Gson();

    JsonElement messageElement = gson.fromJson(message, JsonElement.class);
    return messageElement;
  }

}
