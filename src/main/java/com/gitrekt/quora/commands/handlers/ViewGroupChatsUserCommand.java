package com.gitrekt.quora.commands.handlers;

import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.config.Config;
import com.gitrekt.quora.database.arango.handlers.GroupChatArangoHandler;
import com.gitrekt.quora.models.GroupChat;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;

import java.util.HashMap;

import java.util.List;


public class ViewGroupChatsUserCommand extends Command {

  private static final String[] argumentNames = new String[] {"username"};
  private static final Config CONFIG = Config.getInstance();

  public ViewGroupChatsUserCommand(HashMap<String, Object> args) {
    super(args);
  }

  @Override
  public JsonElement execute() throws Exception {
    checkArguments(argumentNames);

    String username = (String) args.get("username");

    String handlerClassPath = CONFIG.getProperty("arango") + ".GroupChatArangoHandler";
    Class handler = Class.forName(handlerClassPath);
    List<GroupChat> groupchats =
        ((GroupChatArangoHandler) handler.getConstructor().newInstance()).getGroupChats(username);

    Gson gson = new GsonBuilder().create();
    JsonElement groupChatsElements = gson.toJsonTree(groupchats, List.class);
    return groupChatsElements;
  }
}
