package com.gitrekt.quora.commands.handlers;

import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.config.Config;
import com.gitrekt.quora.database.arango.handlers.GroupChatArangoHandler;
import com.gitrekt.quora.models.Message;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;

import java.util.HashMap;

import java.util.List;


public class ViewMessagesGroupChatCommand extends Command {

  private static final String[] argumentNames = new String[] {"chatId"};
  private static final Config CONFIG = Config.getInstance();

  public ViewMessagesGroupChatCommand(HashMap<String, Object> args) {
    super(args);
  }

  @Override
  public JsonElement execute() throws Exception {
    checkArguments(argumentNames);

    String chatId = (String) args.get("chatId");

    String handlerClassPath = CONFIG.getProperty("arango") + ".GroupChatArangoHandler";
    Class handler = Class.forName(handlerClassPath);
    List<Message> groupchatmessages =
        ((GroupChatArangoHandler) handler.getConstructor().newInstance())
            .getGroupChatMessages(chatId);

    Gson gson = new GsonBuilder().create();
    JsonElement groupchatmessagesElements = gson.toJsonTree(groupchatmessages, List.class);
    return groupchatmessagesElements;
  }
}
