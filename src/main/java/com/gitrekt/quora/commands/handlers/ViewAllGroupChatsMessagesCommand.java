package com.gitrekt.quora.commands.handlers;

import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.config.Config;
import com.gitrekt.quora.database.arango.handlers.GroupChatArangoHandler;
import com.google.gson.JsonElement;

import java.util.HashMap;

public class ViewAllGroupChatsMessagesCommand extends Command {

  private static final String[] argumentNames = new String[] {"username"};
  private static final Config CONFIG = Config.getInstance();

  public ViewAllGroupChatsMessagesCommand(HashMap<String, Object> args) {
    super(args);
  }

  @Override
  public JsonElement execute() throws Exception {
    checkArguments(argumentNames);

    String username = (String) args.get("username");

    String handlerClassPath = CONFIG.getProperty("arango") + ".GroupChatArangoHandler";
    Class handler = Class.forName(handlerClassPath);
    JsonElement groupChatsElement =
        ((GroupChatArangoHandler) handler.getConstructor().newInstance())
            .getAllGroupChatsMessages(username);

    return groupChatsElement;
  }
}
