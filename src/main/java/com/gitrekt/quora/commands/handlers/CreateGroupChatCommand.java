package com.gitrekt.quora.commands.handlers;

import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.config.Config;
import com.gitrekt.quora.database.arango.handlers.GroupChatArangoHandler;
import com.google.gson.Gson;
import com.google.gson.JsonElement;

import java.util.HashMap;

public class CreateGroupChatCommand extends Command {

  private static final String[] argumentNames = new String[] {"chatAdmin", "groupChatName"};
  private static final Config CONFIG = Config.getInstance();

  public CreateGroupChatCommand(HashMap<String, Object> args) {
    super(args);
  }

  @Override
  public JsonElement execute() throws Exception {
    checkArguments(argumentNames);

    String chatAdmin = (String) args.get("chatAdmin");
    String groupChatName = (String) args.get("groupChatName");

    String handlerClassPath =
            CONFIG.getProperty("arango") + ".GroupChatArangoHandler";
    Class handler =
        Class.forName(handlerClassPath);
    ((GroupChatArangoHandler) handler.getConstructor().newInstance())
        .createGroupChat(groupChatName, chatAdmin);

    String message = "{\"message\":\"group created successfully\"}";
    Gson gson = new Gson();

    JsonElement messageElement = gson.fromJson(message, JsonElement.class);
    return messageElement;
  }
}
