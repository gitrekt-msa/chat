package com.gitrekt.quora.commands.handlers;

import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.config.Config;
import com.gitrekt.quora.database.arango.handlers.PersonalChatHandler;
import com.gitrekt.quora.models.Message;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;

import java.util.HashMap;
import java.util.List;

public class ViewPersonalChatMessagesCommand extends Command {
  private static final String[] argumentNames = new String[] {"username1", "username2"};
  private static final Config CONFIG = Config.getInstance();

  public ViewPersonalChatMessagesCommand(HashMap<String, Object> args) {
    super(args);
  }

  @Override
  public JsonElement execute() throws Exception {
    checkArguments(argumentNames);

    String username1 = (String) args.get("username1");
    String username2 = (String) args.get("username2");

    String handlerClassPath = CONFIG.getProperty("arango") + ".PersonalChatHandler";
    Class handler = Class.forName(handlerClassPath);
    List<Message> chatMessages =
            ((PersonalChatHandler) handler.getConstructor().newInstance())
                    .getPersonalChatMessages(username1, username2);

    Gson gson = new GsonBuilder().create();
    JsonElement chatMessagesElement = gson.toJsonTree(chatMessages, List.class);
    return chatMessagesElement;
  }
}
