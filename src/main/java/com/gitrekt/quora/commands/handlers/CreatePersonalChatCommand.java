package com.gitrekt.quora.commands.handlers;

import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.config.Config;
import com.gitrekt.quora.database.arango.handlers.PersonalChatHandler;
import com.google.gson.Gson;
import com.google.gson.JsonElement;

import java.util.HashMap;

public class CreatePersonalChatCommand extends Command {
  private static final String[] argumentNames = new String[]{"username1", "username2"};
  private static final Config CONFIG = Config.getInstance();

  public CreatePersonalChatCommand(HashMap<String, Object> args) {
    super(args);
  }

  @Override
  public JsonElement execute() throws Exception {
    checkArguments(argumentNames);

    String username1 = (String) args.get("username1");
    String username2 = (String) args.get("username2");

    String handlerClassPath =
            CONFIG.getProperty("arango") + ".PersonalChatHandler";

    PersonalChatHandler handler = (PersonalChatHandler)
            Class.forName(handlerClassPath).getConstructor().newInstance();

    String response = handler.createPersonalChat(username1, username2);
    String message = "{\"message\":\"" + response + "\"}";
    Gson gson = new Gson();

    JsonElement messageElement = gson.fromJson(message, JsonElement.class);
    return messageElement;
  }
}
