package com.gitrekt.quora.commands.handlers;

import com.arangodb.ArangoDBException;
import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.config.Config;
import com.gitrekt.quora.database.arango.handlers.GroupChatArangoHandler;
import com.google.gson.Gson;
import com.google.gson.JsonElement;

import java.util.HashMap;

public class RemoveMemberGroupChatCommand extends Command {

  private static final String[] argumentNames = new String[] {"chatId", "member"};
  private static final Config CONFIG = Config.getInstance();

  public RemoveMemberGroupChatCommand(HashMap<String, Object> args) {
    super(args);
  }

  @Override
  public JsonElement execute() throws Exception {
    checkArguments(argumentNames);

    String chatId = args.get("chatId").toString();
    String member = (String) args.get("member");

    String handlerClassPath =
            CONFIG.getProperty("arango") + ".GroupChatArangoHandler";
    Class handler =
            Class.forName(handlerClassPath);
    String message = "{\"message\":\""
            + ((GroupChatArangoHandler) handler.getConstructor().newInstance())
        .removeMember(chatId, member) + "\"}";
    Gson gson = new Gson();

    JsonElement messageElement = gson.fromJson(message, JsonElement.class);
    return messageElement;
  }
}
