package com.gitrekt.quora.commands.handlers;

import com.arangodb.ArangoDBException;
import com.gitrekt.quora.commands.Command;
import com.gitrekt.quora.config.Config;
import com.gitrekt.quora.database.arango.handlers.GroupChatArangoHandler;
import com.google.gson.Gson;
import com.google.gson.JsonElement;

import java.util.HashMap;

public class SendMessageGroupChatCommand extends Command {
  private static final String[] argumentNames = new String[] {"chatId", "member", "message"};
  private static final Config CONFIG = Config.getInstance();

  public SendMessageGroupChatCommand(HashMap<String, Object> args) {
    super(args);
  }

  @Override
  public JsonElement execute() throws Exception {

    String chatId = args.get("chatId").toString();
    String member = (String) args.get("member");
    String message = (String) args.get("message");

    String handlerClassPath = CONFIG.getProperty("arango") + ".GroupChatArangoHandler";
    Class handler = Class.forName(handlerClassPath);
    String returnedMessage =
        "{\"message\":\""
            + ((GroupChatArangoHandler) handler.getConstructor().newInstance())
                .addNewMessageInChat(chatId, member, message)
            + "\"}";
    Gson gson = new Gson();

    JsonElement messageElement = gson.fromJson(returnedMessage, JsonElement.class);
    return messageElement;
  }
}
