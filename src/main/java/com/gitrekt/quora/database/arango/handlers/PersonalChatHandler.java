package com.gitrekt.quora.database.arango.handlers;

import com.arangodb.ArangoCursor;
import com.arangodb.ArangoDBException;
import com.arangodb.ArangoDatabase;
import com.arangodb.entity.BaseDocument;
import com.arangodb.util.MapBuilder;
import com.gitrekt.quora.database.arango.ArangoSeed;
import com.gitrekt.quora.models.Message;
import com.gitrekt.quora.models.PersonalChat;

import java.util.ArrayList;
import java.util.Map;

public class PersonalChatHandler extends ArangoHandler<PersonalChat> {
  /**
   * personalChatHandler Constructor.
   */
  public PersonalChatHandler() {
    super("personalChats", PersonalChat.class);
    String seedPath = "src/main/resources/database/nosql/chat/personalChat/personalChat_seed.json";
    new ArangoSeed(seedPath, "personalChats");
  }

  /**
   * query Database method.
   * @param username1 first username
   * @param username2 second username
   * @return arango cursor containing document
   */
  public ArangoCursor<PersonalChat> queryDb(String username1, String username2) {
    String query = "FOR t IN personalChats FILTER "
            + "(t.username1 == @username1 AND t.username2 == @username2) "
            + "OR (t.username2 == @username1 AND t.username1 == @username2) "
            + "RETURN t";
    Map<String, Object> bindVars = new MapBuilder().put("username1", username1).get();
    bindVars.put("username2", username2);
    ArangoCursor<PersonalChat> cursor = connection.db(dbName).query(query, bindVars, null,
            PersonalChat.class);

    return cursor;
  }


  /**
   * getting certain document by 2 user names.
   * @param username1 first username
   * @param username2 second username
   * @return document between these two user names
   */
  public PersonalChat getPersonalChat(String username1, String username2) throws ArangoDBException {
    ArrayList<PersonalChat> list = new ArrayList<>();
    ArangoCursor<PersonalChat> cursor = queryDb(username1, username2);

    while (cursor.hasNext()) {
      PersonalChat personalChat = cursor.next();
      list.add(personalChat);
    }

    return list.get(0);
  }

  /**
   * method for getting chat member.
   * @param chatId chat id
   * @param username1 first username
   * @return chat member username
   */
  public String getChatMember(String chatId, String username1) throws ArangoDBException {
    PersonalChat myDocument =
            connection.db(dbName).collection(collectionName)
                    .getDocument(chatId, PersonalChat.class);

    if (myDocument.getUsername1().equals(username1)) {
      return myDocument.getUsername2();
    }

    return myDocument.getUsername1();
  }

  /**
   * create personal chat document.
   * @param username1 first username
   * @param username2 second username
   * @return success message
   */
  public String createPersonalChat(String username1, String username2) {
    ArangoCursor<PersonalChat> cursor = queryDb(username1, username2);

    if (cursor.hasNext()) {
      return "chat already exists";
    }

    BaseDocument myObject = new BaseDocument();
    myObject.addAttribute("username1", username1);
    myObject.addAttribute("username2", username2);
    super.insertDocument(myObject);

    return "personal chat created successfully";
  }

  /**
   * get chat messages.
   * @param username1 first username
   * @param username2 second username
   * @return list of messages
   */
  public ArrayList<Message> getPersonalChatMessages(String username1, String username2)
          throws ArangoDBException {
    PersonalChat chat = getPersonalChat(username1, username2);
    String query =
            "FOR d IN Message FILTER d.chatId == @chatId "
                    + "AND d.chatType == \"Personal_Chat\" RETURN d";
    return getMessages(chat.getChatId(), query, connection.db(dbName));
  }

  /**
   * getting messages from DB.
   * @param chatId chat id
   * @param query query to be executed
   * @param db arango DB
   * @return
   */
  static ArrayList<Message> getMessages(String chatId, String query, ArangoDatabase db) {
    Map<String, Object> bindingVars = new MapBuilder().put("chatId", chatId).get();
    ArrayList<Message> list = new ArrayList<Message>();
    ArangoCursor<Message> cursor =
            db.query(query, bindingVars, null, Message.class);
    while (cursor.hasNext()) {
      Message message = cursor.next();
      list.add(message);
    }

    return list;
  }

  /**
   * Deleting a chat.
   * @param personalChatId chatID of personalChat
   * @return String that assures that this personalChat has been deleted from the database.
   */
  public String deletePersonalChat(String personalChatId) throws ArangoDBException {

    try {
      connection.db(dbName).collection(collectionName).deleteDocument(personalChatId);
    } catch (ArangoDBException exception) {
      System.out.println(exception.getErrorMessage());
    }

    String query =
            "FOR t IN Message FILTER t.chatId == @chatId AND t.chatType == \"Personal_Chat\""
                    + "REMOVE t in Message";
    Map<String, Object> bindVars = new MapBuilder().put("chatId", personalChatId).get();
    connection.db(dbName).query(query, bindVars, null, BaseDocument.class);

    return "Chat deleted successfully";
  }
}
