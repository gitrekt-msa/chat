package com.gitrekt.quora.database.arango.handlers;

import com.arangodb.ArangoCursor;
import com.arangodb.ArangoDBException;
import com.arangodb.entity.BaseDocument;
import com.arangodb.util.MapBuilder;
import com.gitrekt.quora.database.arango.ArangoSeed;
import com.gitrekt.quora.models.GroupChat;
import com.gitrekt.quora.models.Member;
import com.gitrekt.quora.models.Message;
import com.gitrekt.quora.models.Tuple;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;

import java.util.Date;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

public class GroupChatArangoHandler extends ArangoHandler<GroupChat> {

  /** Constructor for handling group chat. */
  public GroupChatArangoHandler() {
    super("GroupChat", GroupChat.class);
    String seedPath = "src/main/resources/database/nosql/chat/groupChat/groupChat_seed.json";
    new ArangoSeed(seedPath, collectionName);
    new ArangoSeed(seedPath, "Message");
  }

  public Collection<GroupChat> getAllGroupChats() {
    return super.findAll();
  }

  public GroupChat getAGroupChat(String key) {
    return super.findOne(key);
  }

  /**
   * Searching for all the groupchats of this user.
   *
   * @param username Username of user
   * @return List of all groupchats of this user
   */
  public ArrayList<GroupChat> getGroupChats(String username) throws ArangoDBException {
    String query =
        "FOR d IN GroupChat FILTER d.chatAdmin == @username  "
            + "OR @username in d.chatMembers[*].username RETURN d ";
    Map<String, Object> bindingVars = new MapBuilder().put("username", username).get();
    ArrayList<GroupChat> list = new ArrayList<GroupChat>();
    ArangoCursor<GroupChat> cursor =
        connection.db(dbName).query(query, bindingVars, null, GroupChat.class);
    while (cursor.hasNext()) {
      GroupChat groupchat = cursor.next();
      list.add(groupchat);
    }

    return list;
  }

  /**
     * Searching for all the groupchats of this user.
     *
     * @param chatId chatID of groupchat
     * @return ArrayList of all members in this groupchat.
   */
  public ArrayList<String> getGroupChatMembers(String chatId) throws ArangoDBException {
    GroupChat myDocument =
        connection.db(dbName).collection(collectionName).getDocument(chatId, GroupChat.class);
    ArrayList<String> members = new ArrayList<String>();
    members.add(myDocument.getChatAdmin());

    for (int i = 0; i < myDocument.getChatMembers().size(); i++) {
      members.add(myDocument.getChatMembers().get(i).getUsername());
    }
    return members;
  }

  /**
     * Searching for all the groupchats of this user.
     *
     * @param chatId chatID of groupchat
     * @return ArrayList of all Messages of this groupchat.
   **/
  public ArrayList<Message> getGroupChatMessages(String chatId) throws ArangoDBException {
    String query =
        "FOR d IN Message FILTER d.chatId == @chatId AND d.chatType == \"Group_Chat\" RETURN d ";
    Map<String, Object> bindingVars = new MapBuilder().put("chatId", chatId).get();
    ArrayList<Message> list = new ArrayList<Message>();
    ArangoCursor<Message> cursor =
        connection.db(dbName).query(query, bindingVars, null, Message.class);
    while (cursor.hasNext()) {
      Message groupchatmessage = cursor.next();
      list.add(groupchatmessage);
    }

    return list;
  }

  /**
     * Searching for all the groupchats of this user.
     *
     * @param username username of a user
     * @return JsonElemnt that contains all groupchats and messages of this user.
   */
  public JsonElement getAllGroupChatsMessages(String username) throws ArangoDBException {
    ArrayList<GroupChat> groupChats = getGroupChats(username);

    ArrayList<Tuple<GroupChat, ArrayList<Message>>> allGroupChats =
        new ArrayList<Tuple<GroupChat, ArrayList<Message>>>();

    for (int i = 0; i < groupChats.size(); i++) {
      String chatId = groupChats.get(i).getChatId();
      ArrayList<Message> messages = getGroupChatMessages(chatId);
      Tuple<GroupChat, ArrayList<Message>> groupChat =
          new Tuple<GroupChat, ArrayList<Message>>(groupChats.get(i), messages);
      allGroupChats.add(groupChat);
    }
    Gson gson = new GsonBuilder().create();
    JsonElement groupChatsElements = gson.toJsonTree(allGroupChats, ArrayList.class);
    return groupChatsElements;
  }

  /**
   * Creating the group chat.
   *
   * @param groupName Name of the group chat
   * @param chatAdmin Chat admin name
   */
  public void createGroupChat(String groupName, String chatAdmin) throws ArangoDBException {

    BaseDocument myObject = new BaseDocument();
    myObject.addAttribute("groupName", groupName);
    myObject.addAttribute("chatAdmin", chatAdmin);
    myObject.addAttribute("chatMembers", new ArrayList<Member>());
    connection.db(dbName).collection(collectionName).insertDocument(myObject);
    System.out.println("Document created!");
  }

  /**
   * Adding new member to a certain group chat.
   *
   * @param chatId chat Id of the group chat
   * @param newMember username of the new member to be added in the group chat
   */
  public String addMember(String chatId, String newMember) throws ArangoDBException {
    GroupChat myDocument =
        connection.db(dbName).collection(collectionName).getDocument(chatId, GroupChat.class);
    ArrayList<Member> chatMembers = myDocument.getChatMembers();
    boolean flag = false;
    for (int i = 0; i < chatMembers.size(); i++) {
      if (chatMembers.get(i).getUsername().equals(newMember)) {
        flag = true;
        break;
      }
    }
    if (flag == false) {
      Member member = new Member(newMember);
      chatMembers.add(member);
    }
    connection.db(dbName).collection(collectionName).updateDocument(chatId, myDocument);
    System.out.println("New Member added!");

    if (flag) {
      return "Member already exists!";
    } else {
      return "Member added successfully";
    }
  }

  /**
   * Removing new member to a certain group chat.
   *
   * @param chatId Chat Id of the group chat
   * @param member username of the new member to be removed in the group chat
   */
  public String removeMember(String chatId, String member) throws ArangoDBException {
    GroupChat myDocument =
        connection.db(dbName).collection(collectionName).getDocument(chatId, GroupChat.class);
    ArrayList<Member> chatMembers = myDocument.getChatMembers();

    boolean flag = true;

    for (int i = 0; i < chatMembers.size(); i++) {
      if (chatMembers.get(i).getUsername().equals(member)) {
        chatMembers.remove(i);
        flag = false;
        break;
      }
    }
    connection.db(dbName).collection(collectionName).updateDocument(chatId, myDocument);
    System.out.println("member removed!");

    if (flag) {
      return "Member does not exist in this group";
    } else {
      return "Member removed successfully";
    }
  }

  /**
   * Sending certain message to a certain group chat.
   *
   * @param chatId chat Id of the group chat
   * @param username username of the sender of the message
   * @param messageText content of the message that the users want to send to the group chat
   */
  public String addNewMessageInChat(String chatId, String username, String messageText)
      throws ArangoDBException {

    GroupChat myDocument =
        connection.db(dbName).collection(collectionName).getDocument(chatId, GroupChat.class);
    ArrayList<Member> chatMembers = myDocument.getChatMembers();
    String chatAdmin = myDocument.getChatAdmin();
    boolean flag = false;
    if (chatAdmin.equals(username)) {
      flag = true;
    }
    for (int i = 0; i < chatMembers.size() && !flag; i++) {
      if (chatMembers.get(i).getUsername().equals(username)) {
        flag = true;
        break;
      }
    }
    if (flag) {
      Message message =
          new Message(
              chatId,
              username,
              messageText,
              new Timestamp(new Date().getTime()),
              Message.Type.Group_Chat);
      connection.db(dbName).collection("Message").insertDocument(message);
      System.out.println("New Message Created created!");
    }

    if (flag) {
      return "Message added to the Group";
    } else {
      return "Sender does not exist in this group!";
    }
  }

  /**
     * Searching for all the groupchats of this user.
     *
     * @param messages copyOnWriteArrayList of messages
     * @return String that assures that data is added in the database.
   */
  public String addMultipleMessages(CopyOnWriteArrayList<Message> messages) {

    connection.db(dbName).collection("Message").insertDocuments(messages);
    return "added";
  }

  /**
     * Searching for all the groupchats of this user.
     *
     * @param groupChatId chatID of groupchat
     * @return String that assures that this groupchat has been deleted from the database.
   */
  public String deleteGroupChat(String groupChatId, String user) throws ArangoDBException {
    GroupChat myDocument =
            connection.db(dbName).collection(collectionName).getDocument(groupChatId, GroupChat.class);
    String chatAdmin = myDocument.getChatAdmin();
    if (user.equals(chatAdmin)) {
      try {
        connection.db(dbName).collection(collectionName).deleteDocument(groupChatId);
      } catch (ArangoDBException exception) {
        System.out.println(exception.getErrorMessage());
      }

      String query =
          "FOR t IN Message FILTER t.chatId == @chatId AND t.chatType == \"Group_Chat\""
              + "REMOVE t in Message";
      Map<String, Object> bindVars = new MapBuilder().put("chatId", groupChatId).get();
      connection.db(dbName).query(query, bindVars, null, BaseDocument.class);

      return "Chat deleted successfully";
      }
    else{
      return "You are not authorized to delete the groupchat";
    }
  }

  /**
   * Testing GroupChatHandler methods. 1- Creating group chat 2- Adding member to a certain group
   * chat 3- removing a member to a certain group chat 4- sending a message to the group chat
   *
   * @param args Nothing
   */
  public static void main(String[] args) {

    //    GroupChatArangoHandler groupChatArangoHandler = new GroupChatArangoHandler();
    //    groupChatArangoHandler.createGroupChat("Test", "Miky");
    //    String chatId = groupChatArangoHandler.getGroupChats("Miky").get(0).getChatId();
    //    System.out.println(chatId);
    //    groupChatArangoHandler.addMember(chatId, "Mariam");
    //    groupChatArangoHandler.removeMember(chatId, "Mariam");
    //    groupChatArangoHandler.addMember(chatId, "Andrew");
    //
    //    groupChatArangoHandler.addNewMessageInChat(chatId, "Miky", "Hello Everybody");
    //    System.out.println("Finish Updating");
  }
}
