package groupChat_testing;

import static org.junit.Assert.assertEquals;

import com.gitrekt.quora.models.Message;
import org.junit.Before;
import com.gitrekt.quora.database.arango.handlers.GroupChatArangoHandler;
import com.gitrekt.quora.models.GroupChat;
import org.junit.Test;
import com.gitrekt.quora.database.arango.ArangoConnection;
import com.arangodb.ArangoDB;

import com.arangodb.ArangoDBException;

import com.arangodb.entity.CollectionEntity;

import java.util.ArrayList;
import java.util.Collection;


public class GroupChatTest {

    protected ArangoDB connection;
    protected String dbName;
    protected GroupChatArangoHandler handler;

    @Before
    public void init() {
        connection = ArangoConnection.getInstance().getConnection();
        dbName = System.getenv("ARANGO_DB");
        handler = new GroupChatArangoHandler();
        createDatabase();
        createCollection();
    }

    private void createDatabase() {
        Collection<String> dbs = connection.getDatabases();

        for (String db : dbs) {
            if (db.equals(dbName)) {
                return;
            }
        }

        try {
            connection.createDatabase(dbName);
        } catch (ArangoDBException exception) {
            exception.printStackTrace();
        }
    }

    private void createCollection() {
        Collection<CollectionEntity> collections = connection.db(dbName).getCollections();
        for (CollectionEntity entity : collections) {
            if (entity.getName().equals("GroupChat")) {
                connection.db(dbName).collection("GroupChat").drop();
            }
            if (entity.getName().equals("Message")) {
                connection.db(dbName).collection("Message").drop();
            }
        }

        try {
            connection.db(dbName).createCollection("GroupChat");
            connection.db(dbName).createCollection("Message");
        } catch (ArangoDBException exception) {
            exception.printStackTrace();
        }
    }

    @Test
    public void checkGroupIsCreated() {
        String chatAdmin = "Admin";
        String groupName = "Scalable";
        handler.createGroupChat(groupName, chatAdmin);
        ArrayList<GroupChat> result = handler.getGroupChats(chatAdmin);
        assertEquals("The Group chat was not created", result.size(), 1);
        assertEquals("The Group chat was not created with the correct chat admin name", result.get(0).getChatAdmin(), chatAdmin);
        assertEquals("The Group chat was not created with the correct group name", result.get(0).getGroupName(), groupName);
    }

    @Test
    public void checkMemberIsAdded() {
        String chatAdmin = "Admin";
        String groupName = "Scalable";
        String newMember = "Member";
        handler.createGroupChat(groupName, chatAdmin);
        ArrayList<GroupChat> result = handler.getGroupChats(chatAdmin);
        String chatId = result.get(0).getChatId();
        handler.addMember(chatId, newMember);
        ArrayList<GroupChat> results = handler.getGroupChats(chatAdmin);
        assertEquals("The New Member was not added", results.get(0).getChatMembers().size(), 1);
        assertEquals("The New Member was not added with the correct name", results.get(0).getChatMembers().get(0).getUsername(), newMember);
    }

    @Test
    public void checkMemberIsRemoved() {
        String chatAdmin = "Admin";
        String groupName = "Scalable";
        String newMember = "Member";
        handler.createGroupChat(groupName, chatAdmin);
        ArrayList<GroupChat> result = handler.getGroupChats(chatAdmin);
        String chatId = result.get(0).getChatId();
        handler.addMember(chatId, newMember);
        handler.removeMember(chatId, newMember);
        ArrayList<GroupChat> results = handler.getGroupChats(chatAdmin);
        assertEquals("The New Member was not removed", results.get(0).getChatMembers().size(), 0);
    }

    @Test
    public void checkMessageIsAdded() {
        String chatAdmin = "Admin";
        String groupName = "Scalable";
        String newMember = "Member";
        String message = "Test Message";
        handler.createGroupChat(groupName, chatAdmin);
        ArrayList<GroupChat> result = handler.getGroupChats(chatAdmin);
        String chatId = result.get(0).getChatId();
        handler.addMember(chatId, newMember);
        handler.addNewMessageInChat(chatId, newMember, message);
        ArrayList<Message> results = handler.getGroupChatMessages(chatId);
        assertEquals("The New Message was not added", results.size(), 1);
        assertEquals("The New Message was not added with the correct Text", results.get(0).getText(), message);
    }

    @Test
    public void checkGroupChatIsDeleted(){
        String chatAdmin = "Admin";
        String groupName = "Scalable";
        String message = "Test Message";
        handler.createGroupChat(groupName, chatAdmin);
        ArrayList<GroupChat> result = handler.getGroupChats(chatAdmin);
        String chatId = result.get(0).getChatId();
        handler.addNewMessageInChat(chatId, chatAdmin, message);
        int oldGroupChatSize = handler.getGroupChats(chatAdmin).size();
        handler.deleteGroupChat(chatId,chatAdmin);
        int groupChatSize = handler.getGroupChats(chatAdmin).size();
        assertEquals("The groupChat was not deleted",groupChatSize, oldGroupChatSize - 1);
    }





}



