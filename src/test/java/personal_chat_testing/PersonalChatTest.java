package personal_chat_testing;

import static org.junit.Assert.assertEquals;

import com.arangodb.ArangoCursor;
import com.gitrekt.quora.models.Message;
import com.gitrekt.quora.database.arango.handlers.PersonalChatHandler;
import com.gitrekt.quora.database.arango.ArangoConnection;
import com.arangodb.ArangoDB;
import com.arangodb.ArangoDBException;
import com.arangodb.entity.CollectionEntity;
import com.gitrekt.quora.models.PersonalChat;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;

public class PersonalChatTest {
    protected ArangoDB connection;
    protected String dbName;
    protected PersonalChatHandler handler;

    @Before
    public void init() {
        connection = ArangoConnection.getInstance().getConnection();
        dbName = System.getenv("ARANGO_DB");
        handler = new PersonalChatHandler();
        createDatabase();
        createCollection();
    }

    private void createDatabase() {
        Collection<String> dbs = connection.getDatabases();

        for (String db : dbs) {
            if (db.equals(dbName)) {
                return;
            }
        }

        try {
            connection.createDatabase(dbName);
        } catch (ArangoDBException exception) {
            exception.printStackTrace();
        }
    }

    private void createCollection() {
        Collection<CollectionEntity> collections = connection.db(dbName).getCollections();
        for (CollectionEntity entity : collections) {
            if (entity.getName().equals("personalChats")) {
                connection.db(dbName).collection("personalChats").drop();
            }
        }

        try {
            connection.db(dbName).createCollection("personalChats");
        } catch (ArangoDBException exception) {
            exception.printStackTrace();
        }
    }

    @Test
    public void checkChatIsCreated() {
        String username1 = "Andrew";
        String username2 = "Sally";
        handler.createPersonalChat(username1, username2);
        ArrayList<PersonalChat> list = new ArrayList<>();
        ArangoCursor<PersonalChat> cursor = handler.queryDb(username1, username2);

        while (cursor.hasNext()) {
            PersonalChat personalChat = cursor.next();
            list.add(personalChat);
        }
        assertEquals("The personal chat was not created", list.size(), 1);
    }

    @Test
    public void checkPersonalChatIsDeleted(){
        String username1 = "Andrew2";
        String username2 = "Sally2";
        handler.createPersonalChat(username1, username2);
        PersonalChat result = handler.getPersonalChat(username1, username2);
        String chatId = result.getChatId();
        handler.deletePersonalChat(chatId);
        ArrayList<PersonalChat> list = new ArrayList<>();
        ArangoCursor<PersonalChat> cursor = handler.queryDb(username1, username2);

        while (cursor.hasNext()) {
            PersonalChat personalChat = cursor.next();
            list.add(personalChat);
        }
        assertEquals("The personal chat was not created", list.size(), 0);
    }
}
